## **Linfo1002A : Projet 2** => **Site Web Interactif - Groupe E9**
---

## **MEMBERS:**
- Maxence Rencelot
- Thomas Robert
- Guillaume Jadin

## **Pour le lancer :**
1) Il y a d'abords une explication ici : https://sites.uclouvain.be/P2SINF/flask/tutorial/factory.html
ou alors il faut executer les trois commandes suivantes dans un invite de commande  (export correspond à Linux (Attention ce n'est pas le cas pour Windows ou autre):
   - export FLASK_APP=flaskr
   - export FLASK_ENV=development
   - flask run

2) Vous pouvez modifier le code pour enlever cette fonctionnalité (factory), pour ce faire : 
   - Dans les import en début de fichier __init__.py et graphs_generator.py, supprimer les endroits avec écrit "flaskr".
   - Ensuite dans __init__.py, supprimer la fonction create_app et dé-indentez tout le fichier.
   - Rajoutez : test_config=None
   - Changer : app = Flask(__name__, instance_relative_config=True) en app = Flask(__name__)
   - Rajoutez : instance_relative_config=True
   - Enlever le return app en bas et remplacer le par :
   - if __name__ == "__main__":
	    app.run(debug=True)

## **Informations supplémentaires**
Le serveur est dans le fichier __init__.py, c'est une app factory. Sauf si vous changez les fichiers comme décrit
dans l'étape 2. Et celui ci gère les requêtes GET de l'user.
Dans Template se trouve la seule page html.
Dans static se trouve toutes les images et modules materialize pour l'html.
Chaque fichier .py a ensuite des classes et des fonctions généralisées qui cherche du data par requête Sql et le gère.

La database "inginious.sqlite" doit se placer avant le dossier principal dans le même dossier que ce ReadMe.md.
(/!\ Attention si vous changez les fichiers .py comme dans la 2ème façon de faire, il faudra éventuellement bouger celle-ci)
