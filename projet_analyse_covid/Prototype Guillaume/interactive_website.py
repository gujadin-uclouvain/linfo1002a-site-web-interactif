from flask import Flask, render_template
from data_treatment import DataTreatment

app = Flask(__name__)
category_list = ["new_cases", "new_deaths", "total_cases", "total_deaths"]
dt = DataTreatment("Belgium")

@app.route('/')
def index():
    """
    retourne le contenu de la page index.html
    """
    return render_template("covid-19.html", country=dt.country(), data=dt.start())


if __name__ == "__main__":
    app.run(debug=True)
