import csv


class DataTreatment:
    """
    Search data for one country with one category
    Axis-X: Dates
    Axis-Y: Category's values
    """

    def __init__(self, country, category="all"):
        """
        :param country: the country (EN)
        :param category: the category ("new_cases" OR "new_deaths" OR "total_cases" OR "total_deaths" is acceptable)
                         if the entry is empty, the category will be the four categories.
        """
        self.__country = country
        self.__category = category
        self.__file = "templates/full_data.csv"
        self.__datalist = None
        self.__category_info = None
        self.__country_indexes = None

    def country(self):
        """
        Return the name of the chosen country
        :pre: None
        :post: return 'self.__country'
        """
        return self.__country

    def __index_database(self, file):
        """
        CSV FILE CONTENT => NESTED LISTS
        :pre: file: the path of the file
        :post: the content of the file as a list
        """
        with open(file, newline='') as datafile:
            return list(csv.reader(datafile, dialect='excel', delimiter=',', lineterminator='\n'))

    def __find_category_and_index(self, line):
        """
        Verify if the chosen category exist.
            if YES: return [index of the category in data, the category's name]
            if NO: raise TypeError
        if self.__category == "all": return [index of the category in data, the category's name] for all categories in a list
        :pre: line: the first line of the file (the header)
        :post: return [index of the category in data, the category's name] OR for all categories in a list
               raise TypeError
        """
        if self.__category == "all":
            return [[2, self.__translate_category_names(line[2])],
                    [3, self.__translate_category_names(line[3])],
                    [4, self.__translate_category_names(line[4])],
                    [5, self.__translate_category_names(line[5])]]
        for index, elem in enumerate(line):
            if elem == self.__category:
                return [index, self.__translate_category_names(elem)]
        raise TypeError("La categorie choisie n'existe pas dans la base de donnee")

    def __translate_category_names(self, cname):
        """
        Translate a category's name into a user-friendly french term
        :pre: cname: the category's name
        :post: return a user-friendly french term of the category
        """
        redone_cname = ""
        if cname == "new_cases":
            redone_cname = "Nouveaux cas"
        elif cname == "new_deaths":
            redone_cname = "Nouveaux décès"
        elif cname == "total_cases":
            redone_cname = "Total des cas"
        elif cname == "total_deaths":
            redone_cname = "Total des décès"
        return redone_cname

    def __empty_to_zero(self, elem):
        """
        Transform an undefined value (if it is) to a 0
        :pre: elem: the element to transform
        :post: IF elem is an undefined value -> 0
               ELSE -> elem
        """
        if elem == "":
            return '0'
        return elem

    def __binary_country_search(self):
        """
        Search the country's data indexes thanks to the binary search
        :pre: None
        :post: Return the result of the method '__find_one_country_data_with_one_index(self, index)'
        """
        before, after = 1, len(self.__datalist)
        while True:
            middle = before + ((after - before) // 2)
            if before > after:
                raise TypeError("Le pays entree n'est pas dans la base de donnee")
            elif self.__country < self.__datalist[middle][1]:  # 'country' AVANT 'l[middle][1]' dans l'alphabet
                after = middle
            elif self.__country > self.__datalist[middle][1]:  # 'country' APRES 'l[middle][1]' dans l'alphabet
                before = middle
            elif self.__country == self.__datalist[middle][1]:
                return self.__find_one_country_data_with_one_index(middle)

    def __find_one_country_data_with_one_index(self, index):
        """
        Find the others country's data indexes with one of it
        :pre: index: show the index of one country's data
        :post: return [the index of the first data, the index of the last data]
        """
        first, last = index, index
        stop = False
        while not stop:
            if self.__datalist[last][1] == self.__country:
                last += 1
            else:
                stop = True
        stop = False
        while not stop:
            if self.__datalist[first][1] == self.__country:
                first -= 1
            else:
                stop = True
                first += 1
        return [first, last]

    def __pass_zero_values(self, index):
        """
        Verify if the program can pass useless data
        The program is able to compare the four categories at a time
        :pre: index: where the program is in the loop
        :post: Verify if the third element after self.__datalist[index] != 0
                    IF true, return True ELSE return False
        """
        if self.__category == "all":
            if self.__datalist[index + 3][self.__category_info[0][0]] != '0' or self.__datalist[index + 3][self.__category_info[1][0]] != '0' or self.__datalist[index + 3][self.__category_info[2][0]] != '0' or self.__datalist[index + 3][self.__category_info[3][0]] != '0':
                return True
            return False
        else:
            if self.__datalist[index + 3][self.__category_info[0]] != '0':
                return True
            return False

    def __extract_parse_data(self):
        """
        Extract the interest data from 'self.__datalist'
        :pre: None
        :post: the result of the method '__generate_json_data(self, labels, datasets_data)'
        """
        data_date = []
        stop_passing_elements = False
        if self.__category == "all":
            data_category = [[], [], [], []]
        else:
            data_category = []
        for index, line in enumerate(self.__datalist):
            if stop_passing_elements:
                data_date.append(self.__empty_to_zero(line[0]))
                if len(data_category) != 0 and type(data_category[0]) == list:
                    data_category[0].append(self.__empty_to_zero(line[self.__category_info[0][0]]))
                    data_category[1].append(self.__empty_to_zero(line[self.__category_info[1][0]]))
                    data_category[2].append(self.__empty_to_zero(line[self.__category_info[2][0]]))
                    data_category[3].append(self.__empty_to_zero(line[self.__category_info[3][0]]))
                else:
                    data_category.append(self.__empty_to_zero(line[self.__category_info[0]]))
            elif index + 3 >= len(self.__datalist):  # Avoid index out of bound error
                stop_passing_elements = True
                data_date.append(self.__empty_to_zero(line[0]))
                if len(data_category) != 0 and type(data_category[0]) == list:
                    data_category[0].append(self.__empty_to_zero(line[self.__category_info[0][0]]))
                    data_category[1].append(self.__empty_to_zero(line[self.__category_info[1][0]]))
                    data_category[2].append(self.__empty_to_zero(line[self.__category_info[2][0]]))
                    data_category[3].append(self.__empty_to_zero(line[self.__category_info[3][0]]))
                else:
                    data_category.append(self.__empty_to_zero(line[self.__category_info[0]]))
            elif self.__pass_zero_values(index):  # skip values == 0 except the 3 last values
                stop_passing_elements = True
                data_date.append(self.__empty_to_zero(line[0]))
                if len(data_category) != 0 and type(data_category[0]) == list:
                    data_category[0].append(self.__empty_to_zero(line[self.__category_info[0][0]]))
                    data_category[1].append(self.__empty_to_zero(line[self.__category_info[1][0]]))
                    data_category[2].append(self.__empty_to_zero(line[self.__category_info[2][0]]))
                    data_category[3].append(self.__empty_to_zero(line[self.__category_info[3][0]]))
                else:
                    data_category.append(self.__empty_to_zero(line[self.__category_info[0]]))
        return self.__generate_json_data(data_date, data_category)

    def __generate_json_data(self, labels, datasets_data):
        """
        Generate a JSON object for Charts.js
        :pre: labels: data on the axis-x
              datasets: label: legend(s)
              datasets: data: data(s) on the axis-y
        :post: return a JSON object who works with Charts.js
        """
        if self.__category == "all":
            return {"labels": labels, "datasets": [{"label": self.__category_info[0][1], "data": datasets_data[0]},
                                                   {"label": self.__category_info[1][1], "data": datasets_data[1]},
                                                   {"label": self.__category_info[2][1], "data": datasets_data[2]},
                                                   {"label": self.__category_info[3][1], "data": datasets_data[3]}]}
        return {"labels": labels, "datasets": [{"label": self.__category_info[1], "data": datasets_data}]}

    def start(self):
        """
        Execute all of the methods in this class
        :pre: None
        :post: the result of the method '__extract_parse_data(self)'
        """
        self.__datalist = self.__index_database(self.__file)
        self.__category_info = self.__find_category_and_index(self.__datalist[0])
        self.__country_indexes = self.__binary_country_search()
        self.__datalist = self.__datalist[self.__country_indexes[0]:self.__country_indexes[1]]
        return self.__extract_parse_data()
