from flask import Flask, render_template
import csv

with open('full_data.csv',newline='') as mon_fichier:
    global liste
    liste = list(csv.reader((mon_fichier),dialect='excel',delimiter=',',lineterminator='\n'))
    
    i = 1
    while liste[i] != liste[-1]:
        if(liste[i][1] == liste[i+1][1]):
            del liste[i]
        else: i+=1
    
    pays = []
    for i in range(len(liste)):
        pays.append(liste[i][1])
    
    mort = []
    for i in range(len(liste)):
        if (liste[i][5] == ''): mort.append(0)
        else: mort.append(liste[i][5])
    
    cas = []
    for i in range(len(liste)):
        if (liste[i][4] == ''): cas.append(0)
        else: cas.append(liste[i][4])
        
    del pays[0]
    del cas[0]
    del mort[0]
    del pays[-3]   #Del de world afin que l'échelle du graphe soit améliorée
    del cas[-3]
    del mort[-3]
    

app = Flask(__name__)

@app.route('/')
def index():
    """
    retourne le contenu de la page index.html
    """
    return render_template("inde.html", pays_data = pays, cas_data = cas, mort_data = mort)

@app.route('/page.html')
def page():
    """
    retourne le contenu de la page page.html
    """
    return '<!-- contenu de page.html ...'

if __name__ == "__main__":
    app.run(debug=True)