import csv


class DataTreatment:
    """
    Search data for one country with one category
    Axis-X: Dates
    Axis-Y: Category's values
    """

    def __init__(self, country, category="all"):
        """
        :param country: the country (EN)
        :param category: the category ("new_cases" OR "new_deaths" OR "total_cases" OR "total_deaths" is acceptable)
                         if the entry is empty, the category will be the four categories.
        """
        self.__country = country
        self.__category = category
        self.__file = "flaskr/full_data.csv"
        self.__datalist = None
        self.__category_info = None
        self.__country_indexes = None
        self.__axis_x_data = None
        self.__axis_y_data = None
        self.__run()

    def country(self):
        """
        Return the name of the chosen country
        :pre: None
        :post: return 'self.__country'
        """
        return self.__country

    def generate_graph_data(self):
        """
        Generate graph data for Charts.js
        :pre: None
        :post: return a JSON object who works with Charts.js and contains graph data
        """
        if self.__category == "all":
            return {"labels": self.__axis_x_data, "datasets": [{"label": self.__category_info[0][1], "data": self.__axis_y_data[0], "fill": 'false', "backgroundColor": 'false', "borderColor": 'rgb(255, 99, 132)'},
                                                               {"label": self.__category_info[2][1], "data": self.__axis_y_data[2], "fill": 'false', "backgroundColor": 'false', "borderColor": 'rgb(0, 0, 153)'},
                                                               {"label": self.__category_info[1][1], "data": self.__axis_y_data[1], "fill": 'false', "backgroundColor": 'false', "borderColor": 'rgb(128, 0, 128)'},
                                                               {"label": self.__category_info[3][1], "data": self.__axis_y_data[3], "fill": 'false', "backgroundColor": 'false', "borderColor": 'rgb(0, 0, 0)'}
                                                              ]}
        return {"labels": self.__axis_x_data, "datasets": [{"label": self.__category_info[1], "data": self.__axis_y_data, "fill": 'false', "steppedLine": 'false', "backgroundColor": 'false', "borderColor": 'rgb(255, 99, 132)'}]}

    def generate_last_data(self):
        if self.__category == "all":
            return """<table class="highlight centered">
                        <thead>
                            <tr>
                                <th>""" + self.__category_info[0][1] + """</th>
                                <th>""" + self.__category_info[2][1] + """</th>
                                <th>""" + self.__category_info[1][1] + """</th>
                                <th>""" + self.__category_info[3][1] + """</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>""" + self.__axis_y_data[0][-1] + """</td>
                                <td>""" + self.__axis_y_data[2][-1] + """</td>
                                <td>""" + self.__axis_y_data[1][-1] + """</td>
                                <td>""" + self.__axis_y_data[3][-1] + """</td>
                            </tr> 
                        </tbody>
                </table>"""
        else:
            return """<table class="highlight centered">
                                <thead>
                                    <tr>
                                        <th>""" + self.__category_info[1] + """</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>""" + self.__axis_y_data[-1] + """</td>
                                    </tr> 
                                </tbody>
                        </table>"""

    def __index_database(self, file):
        """
        CSV FILE CONTENT => NESTED LISTS
        :pre: file: the path of the file
        :post: the content of the file as a list
        """
        with open(file, newline='') as datafile:
            return list(csv.reader(datafile, dialect='excel', delimiter=',', lineterminator='\n'))

    def __find_category_and_index(self, line):
        """
        Verify if the chosen category exist.
            if YES: return [index of the category in data, the category's name]
            if NO: raise TypeError
        if self.__category == "all": return [index of the category in data, the category's name] for all categories in a list
        :pre: line: the first line of the file (the header)
        :post: return [index of the category in data, the category's name] OR for all categories in a list
               raise TypeError
        """
        if self.__category == "all":
            return [[2, self.__translate_category_names(line[2])],
                    [3, self.__translate_category_names(line[3])],
                    [4, self.__translate_category_names(line[4])],
                    [5, self.__translate_category_names(line[5])]]
        for index, elem in enumerate(line):
            if elem == self.__category:
                return [index, self.__translate_category_names(elem)]
        raise TypeError("La categorie choisie n'existe pas dans la base de donnee")

    def __translate_category_names(self, cname):
        """
        Translate a category's name into a user-friendly french term
        :pre: cname: the category's name
        :post: return a user-friendly french term of the category
        """
        redone_cname = ""
        if cname == "new_cases":
            redone_cname = "New Cases"
        elif cname == "new_deaths":
            redone_cname = "New Deaths"
        elif cname == "total_cases":
            redone_cname = "Total Cases"
        elif cname == "total_deaths":
            redone_cname = "Total Deaths"
        return redone_cname

    def __empty_to_zero(self, elem):
        """
        Transform an undefined value (if it is) to a 0
        :pre: elem: the element to transform
        :post: IF elem is an undefined value -> 0
               ELSE -> elem
        """
        if elem == "":
            return '0'
        return elem

    def __binary_country_search(self):
        """
        Search the country's data indexes thanks to the binary search
        :pre: None
        :post: Return the result of the method '__find_one_country_data_with_one_index(self, index)'
        """
        before, after = 1, len(self.__datalist)
        while True:
            middle = before + ((after - before) // 2)
            if before > after:
                raise TypeError("Le pays entree n'est pas dans la base de donnee")
            elif self.__country < self.__datalist[middle][1]:  # 'country' AVANT 'l[middle][1]' dans l'alphabet
                after = middle
            elif self.__country > self.__datalist[middle][1]:  # 'country' APRES 'l[middle][1]' dans l'alphabet
                before = middle
            elif self.__country == self.__datalist[middle][1]:
                return self.__find_one_country_data_with_one_index(middle)

    def __find_one_country_data_with_one_index(self, index):
        """
        Find the others country's data indexes with one of it
        :pre: index: show the index of one country's data
        :post: return [the index of the first data, the index of the last data]
        """
        first, last = index, index
        stop = False
        while not stop:
            if self.__datalist[last][1] == self.__country:
                last += 1
            else:
                stop = True
        stop = False
        while not stop:
            if self.__datalist[first][1] == self.__country:
                first -= 1
            else:
                stop = True
                first += 1
        return [first, last]

    def __pass_zero_values(self, index):
        """
        Verify if the program can pass useless data
        The program is able to compare the four categories at a time
        :pre: index: where the program is in the loop
        :post: Verify if the third element after self.__datalist[index] != 0
                    IF true, return True ELSE return False
        """
        if self.__category == "all":
            if self.__datalist[index + 3][self.__category_info[0][0]] != '0' or self.__datalist[index + 3][self.__category_info[1][0]] != '0' or self.__datalist[index + 3][self.__category_info[2][0]] != '0' or self.__datalist[index + 3][self.__category_info[3][0]] != '0':
                return True
            return False
        else:
            if self.__datalist[index + 3][self.__category_info[0]] != '0':
                return True
            return False

    def __extract_parse_data(self):
        """
        Extract the interest data from 'self.__datalist'
        :pre: None
        :post: Assign data to "self.__axis_x_data" & "self.__axis_y_data"
        """
        self.__axis_x_data = []
        stop_passing_elements = False
        if self.__category == "all":
            self.__axis_y_data = [[], [], [], []]
        else:
            self.__axis_y_data = []
        for index, line in enumerate(self.__datalist):
            if stop_passing_elements:
                self.__axis_x_data.append(self.__empty_to_zero(line[0]))
                if len(self.__axis_y_data) != 0 and type(self.__axis_y_data[0]) == list:
                    self.__axis_y_data[0].append(self.__empty_to_zero(line[self.__category_info[0][0]]))
                    self.__axis_y_data[1].append(self.__empty_to_zero(line[self.__category_info[1][0]]))
                    self.__axis_y_data[2].append(self.__empty_to_zero(line[self.__category_info[2][0]]))
                    self.__axis_y_data[3].append(self.__empty_to_zero(line[self.__category_info[3][0]]))
                else:
                    self.__axis_y_data.append(self.__empty_to_zero(line[self.__category_info[0]]))
            elif index + 3 >= len(self.__datalist):  # Avoid index out of bound error
                stop_passing_elements = True
                self.__axis_x_data.append(self.__empty_to_zero(line[0]))
                if len(self.__axis_y_data) != 0 and type(self.__axis_y_data[0]) == list:
                    self.__axis_y_data[0].append(self.__empty_to_zero(line[self.__category_info[0][0]]))
                    self.__axis_y_data[1].append(self.__empty_to_zero(line[self.__category_info[1][0]]))
                    self.__axis_y_data[2].append(self.__empty_to_zero(line[self.__category_info[2][0]]))
                    self.__axis_y_data[3].append(self.__empty_to_zero(line[self.__category_info[3][0]]))
                else:
                    self.__axis_y_data.append(self.__empty_to_zero(line[self.__category_info[0]]))
            elif self.__pass_zero_values(index):  # skip values == 0 except the 3 last values
                stop_passing_elements = True
                self.__axis_x_data.append(self.__empty_to_zero(line[0]))
                if len(self.__axis_y_data) != 0 and type(self.__axis_y_data[0]) == list:
                    self.__axis_y_data[0].append(self.__empty_to_zero(line[self.__category_info[0][0]]))
                    self.__axis_y_data[1].append(self.__empty_to_zero(line[self.__category_info[1][0]]))
                    self.__axis_y_data[2].append(self.__empty_to_zero(line[self.__category_info[2][0]]))
                    self.__axis_y_data[3].append(self.__empty_to_zero(line[self.__category_info[3][0]]))
                else:
                    self.__axis_y_data.append(self.__empty_to_zero(line[self.__category_info[0]]))

    def __run(self):
        """
        Execute all of the methods in this class
        :pre: None
        :post: Data Treatment Complete !!!
        """
        self.__datalist = self.__index_database(self.__file)
        self.__category_info = self.__find_category_and_index(self.__datalist[0])
        self.__country_indexes = self.__binary_country_search()
        self.__datalist = self.__datalist[self.__country_indexes[0]:self.__country_indexes[1]]
        self.__extract_parse_data()
