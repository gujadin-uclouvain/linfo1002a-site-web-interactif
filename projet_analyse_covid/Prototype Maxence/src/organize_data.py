import csv

filename = "data/full_data.csv"


def file_to_list(filename=filename):
    """
    Put all information in 'filename' into a list
    :param filename: the name of file with '.csv' extension. By default, the file is "full_data.csv" in the data folder.
    :return: the list with all information of filename
    """
    with open(filename, newline="") as file:
        lst = list(csv.reader(file, dialect='excel', delimiter=',', lineterminator='\n'))
    return lst


def obtain_country(country, lst):
    """
    This function put in a list all information filter by country.
    :param country: the filter to apply on lst who represent a country touched by Covid-19 (str).
    :param lst: The list with all information to filter.
    :return: A filtered list or -1 if the len of list is 0.
    """
    lst_country = []
    for i in range(len(lst)):
        if lst[i][1] == country:
            lst_country.append(lst[i])
    if len(lst_country) == 0:
        print("These country is not in the list !\n")
        return -1
    return lst_country


def obtain_all_country(lst):
    lst_all_country = []
    for i in range(1, len(lst)):
        if lst[i][1] not in lst_all_country:
            lst_all_country.append(lst[i][1])
    return lst_all_country


def obtain_date(lst):
    """
    This function returns all dates contained in 'lst'
    :param lst: A list for one country
    :return: A list with all different date in 'lst
    """
    lst_date = []
    for i in range(len(lst)):
        lst_date.append(lst[i][0])
    return lst_date


def obtain_new_cases_by_country(country, lst):
    lst_country = obtain_country(country, lst)
    lst_new_cases = []
    for i in range(len(lst_country)):
        if lst_country[i][2] == '':
            lst_new_cases.append(0)
        else:
            temp = int(lst_country[i][2])
            lst_new_cases.append(temp)
    return lst_new_cases


def obtain_cases_by_country(country, lst):
    lst_country = obtain_country(country, lst)
    lst_total_case = []
    for i in range(len(lst_country)):
        if lst_country[i][4] == '':
            lst_total_case.append(0)
        else:
            lst_total_case.append(int(lst_country[i][4]))
    return lst_total_case


def obtain_total_deaths_by_country(country, lst):
    lst_country = obtain_country(country, lst)
    lst_total_deaths = []
    for i in range(len(lst_country)):
        if lst_country[i][5] == '':
            lst_total_deaths.append(0)
        else:
            lst_total_deaths.append(int(lst_country[i][5]))
    return lst_total_deaths


def obtain_new_deaths_by_country(country, lst):
    lst_country = obtain_country(country, lst)
    lst_new_deaths = []
    for i in range(len(lst_country)):
        if lst_country[i][3] == '':
            lst_new_deaths.append(0)
        else:
            lst_new_deaths.append(int(lst_country[i][3]))
    return lst_new_deaths
