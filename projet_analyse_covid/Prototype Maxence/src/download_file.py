import requests

"""
    This script is useful to download the file with all information that we need to make graph.
    I use the 'requests' module to search and download the file from Internet.
"""

URL = "https://covid.ourworldindata.org/data/ecdc/full_data.csv"


def download_file(url=URL):
    """
    This function download the file from url and copy it to the data folder as "full_data.csv"
    :param url: An url to find and download the file. By default the url is the link for covid-19 information
    :return: Save the file as "full_data.csv" to the data folder.
    """
    file = requests.get(url, allow_redirects=True)
    open("../data/full_data.csv", "wb").write(file.content)


if __name__ == '__main__':      # Run script
    download_file()

