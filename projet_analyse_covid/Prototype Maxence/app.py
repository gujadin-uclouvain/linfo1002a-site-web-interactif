from flask import Flask, render_template, Markup, request, redirect
import src.organize_data as Data


app = Flask(__name__)


COUNTRIES = Data.obtain_all_country(Data.file_to_list())


@app.route('/')
def index():
    labels = Data.obtain_date(Data.obtain_country('World', Data.file_to_list()))
    values1 = Data.obtain_new_cases_by_country('World', Data.file_to_list())
    values2 = Data.obtain_cases_by_country('World', Data.file_to_list())
    values3 = Data.obtain_new_deaths_by_country('World', Data.file_to_list())
    values4 = Data.obtain_total_deaths_by_country('World', Data.file_to_list())
    return render_template('index.html', labels=labels, values1=values1, values2=values2, values3=values3, values4=values4, title='World', countries=COUNTRIES)

@app.route('/visual', methods = ['POST', 'GET'])
def visual():
    if request.method == 'POST':
        country = str(request.form['Country'])
    else:
        country = request.args.get('Country')


    labels = Data.obtain_date(Data.obtain_country(country, Data.file_to_list()))
    values1 = Data.obtain_new_cases_by_country(country, Data.file_to_list())
    values2 = Data.obtain_cases_by_country(country, Data.file_to_list())
    values3 = Data.obtain_new_deaths_by_country(country, Data.file_to_list())
    values4 = Data.obtain_total_deaths_by_country(country, Data.file_to_list())
    return render_template('index.html', labels=labels, values1=values1, values2=values2, values3=values3, values4=values4, title=country, countries=COUNTRIES)


@app.route('/visual2')
def visual2():
    labels = Data.obtain_date(Data.obtain_country('Italy', Data.file_to_list()))
    values1 = Data.obtain_cases_by_country('Italy', Data.file_to_list())
    values2 = Data.obtain_cases_by_country('Spain', Data.file_to_list())
    return render_template('chart.html', labels=labels, values1=values1, values2=values2, title='Total Cases', first_country='Italy', second_country='Spain', countries=COUNTRIES)


@app.route('/compare', methods = ['POST', 'GET'])
def compare():
    if request.method == 'POST':
        country1 = str(request.form['FirstCountry'])
        country2 = str(request.form['SecondCountry'])
        info = request.form['info']
    else:
        country1 = request.args.get('FirstCountry')
        country2 = request.args.get('SecondCountry')
        info = request.args.get('info')
    
    if(info == "1"):
        values1 = Data.obtain_new_cases_by_country(country1, Data.file_to_list())
        values2 = Data.obtain_new_cases_by_country(country2, Data.file_to_list())
        title = "New Cases"
    elif(info == "2"):
        values1 = Data.obtain_cases_by_country(country1, Data.file_to_list())
        values2 = Data.obtain_cases_by_country(country2, Data.file_to_list())
        title = "Total Case"
    elif(info == "3"):
        values1 = Data.obtain_new_deaths_by_country(country1, Data.file_to_list())
        values2 = Data.obtain_new_deaths_by_country(country2, Data.file_to_list())
        title = "New Deaths"
    else:
        values1 = Data.obtain_total_deaths_by_country(country1, Data.file_to_list())
        values2 = Data.obtain_total_deaths_by_country(country2, Data.file_to_list())
        title = "Total Deaths"
    labels = Data.obtain_date(Data.obtain_country('China', Data.file_to_list()))
    return render_template('chart.html', labels=labels, values1=values1, values2=values2, title=title, first_country=country1, second_country=country2, countries=COUNTRIES)

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8003)

# Rendre la page plus belle.
# Animation sur le grapique.
# Callback si erreur

# Comparer deux pays en graphique mais donner le choix des paramètres.
# Graphique en batonnet avec tous les pays mais choix de la catégories.