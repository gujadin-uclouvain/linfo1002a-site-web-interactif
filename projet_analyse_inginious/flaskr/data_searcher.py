import sqlite3


class SearchDataForCartesianGraph:
    def __init__(self, type_axis_x, name_axis_x, name_axis_y, options_axis_y, sql_requests, is_two_y_axe=False, name_axis2_y=None):
        """
        Implementation generique qui rend plus facile la recherche SQL et l ajout des donnees vers Javascript

        :param type_axis_x: the data type on axis x
        :param name_axis_x: the data name on axis x
        :param name_axis_y: the data name on axis y
        :param options_axis_y: a LIST of DICT and for each DICT, the options for one dataset AND the key "data" with the value [].
        :param sql_requests: a LIST of STR with all the SQL requests.
        """
        self.__db, self.__cursor = None, None
        self.name_type_axis = self.__options_generator(type_axis_x, name_axis_x, name_axis_y, is_two_y_axe, name_axis2_y)
        self.data_axis_y = options_axis_y
        self.data_axis_x = []
        self.sql_requests = sql_requests

    def __connect_db(self):
        """
        Connecte la DB au fichier 'inginious.sqlite'
        """
        if self.__db is None:
            self.__db = sqlite3.connect('./inginious.sqlite')  # Accès à la base de données
            self.__cursor = self.__db.cursor()

    def __close_db(self):
        """
        Ferme et reinitialise la DB
        """
        if self.__db is not None:
            self.__db.close()
            self.__db, self.__cursor = None, None

    @staticmethod
    def __options_generator(type_x, name_x, name_y, is_two_y_axe=False, name2_y=None):
        """
        Generate options for the graph

        :param type_x: the data type on axis x
        :param name_x: the data name on axis x
        :param name_y: the data name on axis y
        :return: a DICT with all the options of the graph
        """
        if type_x == "":
            type_x = 'category'
        if is_two_y_axe:
            return {
            "responsive": True,
            "scales": {
                "xAxes": [{
                    "type": type_x,
                    "display": True,
                    "scaleLabel": {
                        "display": True,
                        "fontSize": '15',
                        "fontStyle": 'bold',
                        "labelString": name_x
                    },
                    "ticks": {
                        "fontSize": '13',
                        "major": {
                            "fontStyle": 'bold',
                            "fontColor": '#FF0000'
                        }
                    }
                }],
                "yAxes": [{
                    "id": 'y-axis-1',
                    "display": True,
                    "position": 'left',
                    "scaleLabel": {
                        "display": True,
                        "fontSize": '15',
                        "fontStyle": 'bold',
                        "labelString": name_y
                    }
                }, {
                    "id": 'y-axis-2',
                    "display": True,
                    "position": 'right',
                    "scaleLabel": {
                        "display": True,
                        "fontSize": '15',
                        "fontStyle": 'bold',
                        "labelString": name2_y
                    },
                    "ticks": {
                        "max": 100,
                        "min": 0
                    },
                    "gridLines": {
                        "drawOnChartArea": False,
                    }
                }]
            }
        }
        else:
            return {
            "responsive": True,
            "scales": {
                "xAxes": [{
                    "type": type_x,
                    "display": True,
                    "scaleLabel": {
                        "display": True,
                        "fontSize": '15',
                        "fontStyle": 'bold',
                        "labelString": name_x
                    },
                    "ticks": {
                        "fontSize": '13',
                        "major": {
                            "fontStyle": 'bold',
                            "fontColor": '#FF0000'
                        }
                    }
                }],
                "yAxes": [{
                    "id": 'y-axis-1',
                    "display": True,
                    "position": 'left',
                    "scaleLabel": {
                        "display": True,
                        "fontSize": '15',
                        "fontStyle": 'bold',
                        "labelString": name_y
                    }
                }]
            }
        }

    def run_for_tot_graph(self):
        """
        Run the SQL search with the inputs in the __init__ method:
        - For the 3th graph ONLY
        - One SQL request ONLY
        - First elem => LABELS
        - Others elems => DATA

        :return: {'x': labels, 'y': data}
        """
        self.__connect_db()
        x = ["0-25", "25-50", "50-75", "75-100"]
        y = [0, 0, 0, 0]
        tot = 0
        for row in self.__cursor.execute(self.sql_requests):
            if 0 <= row[0] <= 25:
                y[0] += 1
            elif 25 < row[0] <= 50:
                y[1] += 1
            elif 50 < row[0] <= 75:
                y[2] += 1
            elif 75 < row[0] <= 100:
                y[3] += 1
            tot += 1
        if tot != 0:
            self.data_axis_x = x
            for counter in y:
                self.data_axis_y[0]["data"].append(str(round((counter/tot)*100, 1)))
            self.__close_db()
        return {'x': self.data_axis_x, 'y': self.data_axis_y}

    def run_with_one_request(self, is_two_y_axe=False):
        """
        Run the SQL search with the inputs in the __init__ method:
        - One SQL request ONLY
        - First elem => X DATA
        - Others elems => Y DATA(S)

        :return: {'x': data on the axis X, 'y': data(s) on the axis Y, 'options': options of the graph}
        """
        if type(self.sql_requests) is not str:
            if type(self.sql_requests) is list:
                wrong_method_msg = " (use 'run_with_mult_requests()' method instead)"
            else:
                wrong_method_msg = ""
            raise Exception(
                "sql_requests must be a <class 'str'> not a {}{}".format(type(self.sql_requests), wrong_method_msg))
        self.__connect_db()
        for row in self.__cursor.execute(self.sql_requests):
            if len(row) - 1 != len(self.data_axis_y) and (not is_two_y_axe):
                raise ValueError(
                    "The Y data configuration number (GRAPH OPTIONS) must be equal to the number of result_type (SQL)")
            self.data_axis_x.append(str(row[0]))
            for nbr_data in range(len(self.data_axis_y)):
                if nbr_data == len(self.data_axis_y)-1 and is_two_y_axe and len(row) == len(self.data_axis_y):
                    self.data_axis_y[nbr_data]["data"].append(str(round((row[2]/row[1])*100, 1)))
                elif is_two_y_axe and len(row) != len(self.data_axis_y):
                    raise ValueError("The Y data configuration number (+1 FOR THE SECOND Y AXIS) (GRAPH OPTIONS) must be equal to the number of result_type (SQL)")
                else:
                    self.data_axis_y[nbr_data]["data"].append(str(row[nbr_data + 1]))

        self.__close_db()
        return {'x': self.data_axis_x, 'y': self.data_axis_y, 'options': self.name_type_axis}

    def run_with_mult_requests(self):
        """
        Run the SQL search with the inputs in the __init__ method:
        - First SQL request ONLY SAVE in the X Data,
        - One SQL request SAVE One Y Data
        - A line which return the SQL request MUST BE => (DATA_X, DATA_Y, OTHER)

        :pre: len(self.sql_requests) == len(self.data_axis_y)
        :return: {'x': data on the axis X, 'y': data(s) on the axis Y, 'options': options of the graph}
        """
        if type(self.sql_requests) is not list:
            if type(self.sql_requests) is str:
                wrong_method_msg = " (use 'run_with_one_request()' method instead)"
            else:
                wrong_method_msg = ""
            raise Exception(
                "sql_requests must be a <class 'list'> not a {}{}".format(type(self.sql_requests), wrong_method_msg))
        if len(self.sql_requests) != len(self.data_axis_y):
            raise Exception(
                "Le nombre de requetes SQL (input 3) == nombre de dictionnaires dans 'options_axis_y' (input 2)")
        self.__connect_db()
        for nbr, sql_request in enumerate(self.sql_requests):
            for row in self.__cursor.execute(sql_request):
                if nbr == 0:
                    self.data_axis_x.append(str(row[0]))
                self.data_axis_y[nbr]["data"].append(str(row[1]))
        self.__close_db()
        return {'x': self.data_axis_x, 'y': self.data_axis_y, 'options': self.name_type_axis}
