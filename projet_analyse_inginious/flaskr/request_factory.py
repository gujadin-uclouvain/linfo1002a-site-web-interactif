class RequestFactory:
    """
    Classe qui genere des requetes SQL sous forme de string
    """
    @staticmethod
    def nbr_submissions_DATE_RESULT(type_result, start_date="", end_date=""):
        """
        [ONE SQL REQUEST]
        Affiche le nombre de soumissions de 'start_date' à 'end_date' qui ont été 'type_result'
        :param type_result: liste contenant un ou plusieurs de ces elements: all, failed, killed, success, overflow, timeout, crash, error ou NULL
        :param start_date: la première date à prendre pour la recherche (premiere date si rien)
        :param end_date: la dernière date à prendre pour la recherche (derniere date si rien)
        :return: 1 LINE => 1 + len(type_result)-1 VALUES
        """
        def generate_where_clause():
            if start_date == "" and end_date == "":
                where_clause = ""
            else:
                where_clause = "WHERE "
                if start_date != "":
                    where_clause = where_clause + 'submitted_on>' + "'{}'".format(start_date)
                    if end_date != "":
                        where_clause = where_clause + ' AND submitted_on<' + "'{}'".format(end_date)
                elif end_date != "":
                    where_clause = where_clause + 'submitted_on<' + "'{}'".format(end_date)
            return where_clause

        def generate_result_calls():
            result_calls = ""
            if type(type_result) == list:
                if len(type_result) == 0:
                    raise ValueError("Minimum One type of result is needed in the list")
                for index, elem in enumerate(type_result):
                    if elem not in ('all', 'success', 'failed', 'killed', 'overflow', 'timeout', 'crash', 'error', 'NULL'):
                        raise ValueError("{} is an unacceptable value".format(elem))
                    if elem == "all":
                        result_calls += "COUNT(result)"
                    else:
                        result_calls += "SUM(CASE WHEN result='{}' THEN 1 ELSE 0 END)".format(elem)
                    if len(type_result) - 1 != index:
                        result_calls += ", "
            else:
                raise TypeError("'type_result' need to be a <class 'list'>, not a {}".format(type(type_result)))
            return result_calls

        return """
        SELECT date(substr(submitted_on,0,24)), {}
        FROM submissions
        {}
        GROUP BY date(substr(submitted_on,0,24)) ORDER BY submitted_on
        """.format(generate_result_calls(), generate_where_clause())

    @staticmethod
    def nbr_submissions_tried_and_successed(course_id):
        """
        Affiche le nombre d'essais [1] et de soumissions reussies [2] pour toutes les taches [0] d'un cours (course_id)
        :param course_id: l'ID d'un cours
        """
        if course_id != "":
            course_id = "WHERE course=" + "'{}'".format(course_id)
        return """
        SELECT task,
        sum(tried),
        sum(case when succeeded='true' then 1 else 0 end)
        FROM user_tasks 
        {} GROUP BY task ORDER BY sum(tried)""".format(course_id)

    @staticmethod
    def perc_win_course(course_id):
        """
        Affiche la moyenne de réussite des eleves pour un cours (course_id)
        :param course_id: l'ID d'un cours
        """
        if course_id != "":
            course_id = "WHERE course=" + "'{}'".format(course_id)
        return """SELECT 
        (sum(grade)/count(grade))
        FROM user_tasks 
        {} GROUP BY username""".format(course_id)
