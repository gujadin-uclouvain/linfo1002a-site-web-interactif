function send_course_id(course_id) {
    let id = document.getElementById('BUTTON NAME');
    id.value = course_id;
    document.forms['FORM NAME'].submit();
}

function disable_box(id_box) {
    const id = document.getElementById(id_box);
    if ( id.style.display === "block" ) { id.style.display = "none"; }
    else { id.style.display = "block"; }
}

//Sidenav
document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.sidenav');
    var instances = M.Sidenav.init(elems, {
        draggable: true
    });
});

//Datepicker
document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.datepicker');
    var instances = M.Datepicker.init(elems, {
        showClearBtn: true,
        firstDay: 1, // Begin the Week with the Monday
        defaultDate: new Date(2019, 7, 1),
        minDate: new Date(2019, 1, 4),
        maxDate: new Date(2020, 1, 17),
        selectMonths: true, // Enable Month Selection
        selectYears: 1, // Creates a dropdown of 1 years to control year
        format:'yyyy-mm-dd',
    });
});

//Select
document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('select');
    var instances = M.FormSelect.init(elems, {});
});

//Autocomplete
document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.autocomplete');
    var instances = M.Autocomplete.init(elems, {
        data: id_courses,
        limit: 5,
        minLength: 0,
    });
});

const id_courses = {
'LEPL1402': null,
'LSINF1101-PYTHON': null,
'LSINF1252': null
}