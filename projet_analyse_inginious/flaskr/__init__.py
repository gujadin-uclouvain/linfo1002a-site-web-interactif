import os

from flask import Flask, render_template, request
from flaskr.graphs_generator import GraphsGenerator
from flaskr.tools import date_parser
from flaskr.tools import SettingsGenerator


def create_app(test_config=None):
    # create and configure the app
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        SECRET_KEY='dev',
        DATABASE=os.path.join(app.instance_path, 'inginious.sqlite'),
    )

    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile('config.py', silent=True)
    else:
        # load the test config if passed in
        app.config.from_mapping(test_config)

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    @app.route('/')
    def index():
        """
        Main Page
        :return: Render the "index.html" page
        """
        settings = SettingsGenerator.for_main_page()
        return render_template(
            "index.html",
            is_main_page=True,
            settings=settings,
            has_correct_input=False
        )

    @app.route('/all')
    def all():
        """
        Global Submissions
        :return: Render the "index.html" page
        """
        data = GraphsGenerator().nbr_submissions_date_result(request.args.get("start_date"), request.args.get("end_date"))
        try:
            start_date, end_date = data['x'][0], data['x'][-1]
        except IndexError:
            start_date, end_date = None, None
        start_date_parsed = date_parser(start_date, end_date)
        start_date_parsed, end_date_parsed = start_date_parsed["start_date"], start_date_parsed["end_date"]
        title = 'Submissions from <b>' + start_date_parsed + '</b> to <b>' + end_date_parsed + '</b>'
        settings = SettingsGenerator.for_start_date_and_end_date(start_date, end_date)
        return render_template(
            "index.html",
            title=title,
            type_title="Global Submissions",
            has_correct_input=False if start_date is None else True,
            settings=settings,
            type_graph="line",
            axis_x_data=data['x'],
            axis_y_data=data['y'],
            options=data['options']
            )

    @app.route('/try')
    def win():
        """
        Submissions Per Course
        :return: Render the "index.html" page
        """
        id_course = request.args.get("id_course")
        data = GraphsGenerator().nbr_submissions_tried_and_successed(id_course)
        title = 'Tried and succeded submissions numbers for <b>{}</b>'.format(id_course)
        settings = SettingsGenerator.for_id_course(None, "/try")
        return render_template(
            "index.html",
            title=title,
            type_title="Submissions Per Course",
            has_correct_input=False if len(data["x"]) == 0 else True,
            settings=settings,
            type_graph="bar",
            axis_x_data=data['x'],
            axis_y_data=data['y'],
            options=data['options']
            )

    @app.route('/avg')
    def avg():
        """
        Percentage Of Success
        :return: Render the "index.html" page
        """
        id_course = request.args.get("id_course")
        data = GraphsGenerator().perc_win_course(id_course)
        settings = SettingsGenerator.for_id_course(None, "/avg")
        return render_template(
            "index.html",
            title="Percentage Of {} Success of the <b>{}</b> course".format("Overall", id_course),
            type_title="Percentage Of Success",
            has_correct_input=False if len(data["x"]) == 0 else True,
            settings=settings,
            type_graph="polarArea",
            axis_x_data=data['x'],
            axis_y_data=data['y'],
            options=None
        )
    return app
