import datetime


def date_parser(start_date, end_date):
    if start_date == 'None' or start_date is None:
        start_date_parsed = ""
    else:
        start_date_parsed = start_date.split("-")
        start_date_parsed = datetime.datetime(int(start_date_parsed[0]), int(start_date_parsed[1]),
                                              int(start_date_parsed[2]))
        start_date_parsed = '{:%B %d %Y}'.format(start_date_parsed)

    if end_date == 'None' or end_date is None:
        end_date_parsed = ""
    else:
        end_date_parsed = end_date.split("-")
        end_date_parsed = datetime.datetime(int(end_date_parsed[0]), int(end_date_parsed[1]), int(end_date_parsed[2]))
        end_date_parsed = '{:%B %d %Y}'.format(end_date_parsed)
    return {'start_date': start_date_parsed, 'end_date': end_date_parsed}


class SettingsGenerator:
    @staticmethod
    def for_main_page():
        return """
<div class="container">
    <h4 class="inginious_style">Introduction :</h4>
    <div class="divider"></div>
    <br>
    <p style="text-align: justify"> Welcome to our INGInious Analysis website,
    <br><br>
    The main objective of this site is to be able to visualize the information collected by INGInious through several
    interactive and dynamic graphs.
    This information is presented in a clear and interactive way to make studying these data more comfortable and enjoyable.
    </p>
    <br><br>
    <h4 class="inginious_style">Different Visualizations :</h4>
    <div class="divider"></div>
    <br>
    <div class="flex-container">
        <div class="card carte grey card-panel lighten-3">
            <div class="card-image waves-effect waves-block waves-light">
                <img class="activator" src="../static/graph1.jpg" alt="">
            </div>
            <div class="card-content">
                <span class="inginious_style card-title activator grey-text text-darken-4">Global Submissions<i class="material-icons right">more_vert</i></span>
                <p><a href="/all">Access the graph</a></p>
            </div>
            <div class="card-reveal">
                <span class="card-title grey-text-darken-4">Global Submissions<i class="material-icons right">close</i></span>
                <div class="divider"></div>
                <p style="text-align: justify">This section allows you to view the submissions made on INGInious between
                 two dates thanks to an evolving graph. This includes, total submissions, valid submissions and failed submissions.
                 <br><br>
                 In addition, you are completely free to change the dates as you wish.
                 </p>
            </div>
        </div>
        <div class="card carte grey card-panel lighten-3">
            <div class="card-image waves-effect waves-block waves-light">
                <img class="activator" src="../static/graph2.jpg" alt="">
            </div>
            <div class="card-content">
                <span class="inginious_style card-title activator grey-text text-darken-4">Submissions Per Course<i class="material-icons right">more_vert</i>
                </span>
                <p><a href="/try">Access the graph</a></p>
            </div>
            <div class="card-reveal">
                <span class="card-title grey-text text-darken-4">Submissions Per Course<i class="material-icons right">close</i></span>
                <div class="divider"></div>
                <p style="text-align: justify">this section allows you to view the total submissions and successful
                submissions for each task in a course using a bar graph.
                <br><br>
                In addition, you are free to choose the course you want to see.
                </p>
            </div>
        </div>
        <div class="card carte grey card-panel lighten-3">
            <div class="card-image waves-effect waves-block waves-light">
                <img class="activator" src="../static/graph3.jpg" alt="">
            </div>
            <div class="card-content">
                <span class="inginious_style card-title activator grey-text text-darken-4">Percentage Of Success<i class="material-icons right">more_vert</i>
                </span>
                <p><a href="/avg">Access the graph</a></p>
            </div>
            <div class="card-reveal">
                <span class="card-title grey-text text-darken-4">Percentage Of Success<i class="material-icons right">close</i></span>
                <div class="divider"></div>
                <p style="text-align: justify">This section allows you to view the percentage of success of a course. By letting you choose the course to view.</p>
            </div>
        </div>
    </div>
</div>
"""


    @staticmethod
    def for_start_date_and_end_date(start_date, end_date):
        val_sd = start_date if start_date is not None else ''
        val_ed = end_date if end_date is not None else ''
        return """
<div class="row">
    <form action="/all" method="get">
        <h3 class="hover_green center inginious_style hover_underline" onclick="disable_box('settings_box')"><b>Settings</b></h3>
        <div id="settings_box" style="display: block" class="container">
            <br><br>
            <div class="row">
                <div class="input-field col s6">
                    <input type="text" class="datepicker validate" id="start_date" name="start_date" autocomplete="off" value='""" + val_sd + """' required>
                    <label class="unselected" for="start_date">Start Date</label>
                    <span class="helper-text"></span>
                </div>
                <div class="input-field col s6">
                    <input type="text" class="datepicker validate" id="end_date" name="end_date" autocomplete="off" value='""" + val_ed + """' required>
                    <label class="unselected" for="end_date">End Date</label>
                    <span class="helper-text"></span>
                </div>
                <div class="center">
                    <button class="btn center orange pin-under" type="submit">Submit</button>
                </div>
                <br>
            </div>
        </div>
    </form>
</div>
"""

    @staticmethod
    def for_id_course(id_course, action):
        val_ic = id_course if id_course is not None else ''
        return """
<div class="row">
    <form action='""" + action + """' method="get">
        <h3 class="hover_green center inginious_style hover_underline" onclick="disable_box('settings_box')"><b>Settings</b></h3>
        <div id="settings_box" style="display: block" class="container">
            <br><br>
            <div class="row">
                <div class="input-field col s12">
                    <input type="text" class="autocomplete validate" id="id_course" name="id_course" autocomplete="off" value='""" + val_ic + """' required>
                    <label class="unselected" for="id_course">Course ID</label>
                    <span class="helper-text"></span>
                </div>
                <div class="center">
                    <button class="btn center orange pin-under" type="submit">Submit</button>
                </div>
                <br>
            </div>
        </div>
    </form>
</div>
"""
