from flaskr.data_searcher import SearchDataForCartesianGraph
from flaskr.request_factory import RequestFactory
import random


def rgb_randomizer():
    """
    Genere un liste avec des couleurs a base de rouge, vert et bleu

    :return: une liste avec differentes couleurs
    """
    r = random.randint(30, 60)
    return [
        "255, " + str(r * 1) + ", " + str(r),
        str(r - 30) + ", " + str(r + 160) + ", " + str(r - 30),
        str(r) + ", " + str(r) + ", 200",
        str(r - 10) + ", " + "255, " + str(r * 3)
    ]


class GraphsGenerator:
    """
    Genere les donnees des graphiques
    """
    def __init__(self):
        shade = rgb_randomizer()
        self.blue, self.green, self.red, self.green_light = shade[2], shade[1], shade[0], shade[3]

    def nbr_submissions_date_result(self, start_date, end_date):
        """
        Data du graphe "Global Submissions"

        :param start_date: la date de depart
        :param end_date:  la date de fin
        :return: un dictionnaire avec x, y et options comme key
        """
        sdfcg = SearchDataForCartesianGraph(
            type_axis_x='time',
            name_axis_x='Date',
            name_axis_y='# submissions',
            name_axis2_y='% submissions',
            is_two_y_axe=True,
            options_axis_y=[
                {"label": "All submissions", "yAxisID": 'y-axis-1', "data": [], "fill": False,
                 "backgroundColor": 'rgba(' + self.blue + ', 0.1)',
                 "borderColor": 'rgb(' + self.blue + ')'},
                {"label": "Valid submissions", "yAxisID": 'y-axis-1', "data": [], "fill": True,
                 "backgroundColor": 'rgba(' + self.green + ', 0.1)',
                 "borderColor": 'rgb(' + self.green + ')'},
                {"label": "Failed submissions", "yAxisID": 'y-axis-1', "data": [], "fill": True,
                 "backgroundColor": 'rgba(' + self.red + ', 0.1)',
                 "borderColor": 'rgb(' + self.red + ')'},
                {"label": "Valid Percentage", "yAxisID": 'y-axis-2', "data": [], "fill": False,
                 "backgroundColor": 'rgba(' + self.green_light + ', 0.1)',
                 "borderColor": 'rgb(' + self.green_light + ')'},
            ],
            sql_requests=RequestFactory().nbr_submissions_DATE_RESULT(['all', 'success', 'failed'], start_date,
                                                                      end_date)
        )
        return sdfcg.run_with_one_request(is_two_y_axe=True)

    def nbr_submissions_tried_and_successed(self, course_id):
        """
        Data du graphe "Submissions Per Course"

        :param course_id: l'ID du cours
        :return: un dictionnaire avec x, y et options comme key
        """
        sdfcg = SearchDataForCartesianGraph(
            type_axis_x='',
            name_axis_x='Tasks',
            name_axis_y='# submissions',
            options_axis_y=[
                {"label": "Tried submissions", "data": [], "fill": False,
                 "backgroundColor": 'rgb(' + self.blue + ')',
                 "borderColor": 'rgba(' + self.blue + ', 0.1)'},
                {"label": "Success submissions", "data": [], "fill": True,
                 "backgroundColor": 'rgb(' + self.green + ')',
                 "borderColor": 'rgba(' + self.green + ', 0.1)'},
            ],
            sql_requests=RequestFactory().nbr_submissions_tried_and_successed(course_id)
        )
        return sdfcg.run_with_one_request()

    def perc_win_course(self, course_id):
        """
        Data du graphe "Percentage Of Success"

        :param course_id: l'ID du cours
        :return: un dictionnaire avec x et y comme key
        """
        sdfcg = SearchDataForCartesianGraph(
            type_axis_x='',
            name_axis_x='Tasks',
            name_axis_y='Average',
            options_axis_y=[
                {"label": "Top tasks", "data": [], "fill": False,
                 "backgroundColor": ['rgb(' + self.green_light + ', 0.5)', 'rgb(' + self.red + ', 0.5)',
                                     'rgb(' + self.blue + ', 0.5)', 'rgb(' + self.green + ', 0.5)'],
                 "borderColor": 'rgb(' + self.blue + ')'},
            ],
            sql_requests=
            RequestFactory().perc_win_course(course_id)
        )
        return sdfcg.run_for_tot_graph()
